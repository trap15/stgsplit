#include "top.h"

#include "getimg.h"
#include "deccfg.h"
#include "imgdec.h"

#include "ini.h"

int xtoi_full(char* str, int base, char** sstr);
int xtoi_bs(char* str, int base);
int xtoi(char* str);

Deccfg::Deccfg()
{
  /* Font */
  font.file = NULL;
  font.w = -1;
  font.h = -1;
  font.scale = -1;
  font.thresh = -1;

  /* Filter */
  filter.scale  = -1;
  filter.blur   = -1;
  filter.thresh = -1;

  /* ex */
  filterex.deriv = -1;
  filterex.ksize = -1;
  filterex.scale = -1;
  filterex.delt = -1;
  filterex.weight = -1;
  filterex.mode = -1;

  /* Edger */
  edger.area = -1;
  edger.thresh = -1;

  edger.adj_x = -11111;
  edger.adj_y = -11111;
  edger.adj_w = -11111;
  edger.adj_h = -11111;

  /* ex */
  edgerex.deriv = -1;
  edgerex.ksize = -1;
  edgerex.scale = -1;
  edgerex.delt = -1;
  edgerex.weight = -1;
}

Deccfg::~Deccfg()
{

}

void Deccfg::apply_unsets()
{
  /* Filter */
  if(filter.scale  < 0) filter.scale  = 2.0;
  if(filter.blur   < 0) filter.blur   = filter.scale * 2.0;
  if(filter.thresh < 0) filter.thresh = 0x80;

  /* ex */
  if(filterex.deriv  < 0) filterex.deriv  = 3;
  if(filterex.ksize  < 0) filterex.ksize  = 5;
  if(filterex.scale  < 0) filterex.scale  = 20.0;
  if(filterex.delt   < 0) filterex.delt   = 0.0;
  if(filterex.weight < 0) filterex.weight = 1.0;
  if(filterex.mode   < 0) filterex.mode   = CV_TM_CCORR_NORMED;

  /* Edger */
  if(edger.area   < 0) edger.area   = 200;
  if(edger.thresh < 0) edger.thresh = filter.thresh;
  if(edger.adj_x == -11111) edger.adj_x  = -8;
  if(edger.adj_y == -11111) edger.adj_y  = -8;
  if(edger.adj_w == -11111) edger.adj_w  = 16;
  if(edger.adj_h == -11111) edger.adj_h  = 16;

  /* ex */
  if(edgerex.deriv  < 0) edgerex.deriv  = filterex.deriv;
  if(edgerex.ksize  < 0) edgerex.ksize  = filterex.ksize;
  if(edgerex.scale  < 0) edgerex.scale  = filterex.scale;
  if(edgerex.delt   < 0) edgerex.delt   = filterex.delt;
  if(edgerex.weight < 0) edgerex.weight = filterex.weight;

  /* Font */
  if(font.file == NULL) {
    font.file = make_path("font.png");
  }
  if(font.w <= 0) font.w = 8;
  if(font.h <= 0) font.h = 8;
  if(font.scale < 0) font.scale = 2.0;
  if(font.thresh < 0) font.thresh = filter.thresh;
}

static char* _cfg_get_path(Deccfg* cfg, const char* file)
{
  return cfg->make_path((char*)file);
}

static int _cfg_get_num(Deccfg* cfg, const char* value)
{
  return xtoi((char*)value);
}

static double _cfg_get_dbl(Deccfg* cfg, const char* value)
{
  return strtod(value, NULL);
}

#define MATCH(s,n) ((strcmp(section,s) == 0) && (strcmp(name,n) == 0))
static int cfg_handler(void* user, const char* section, const char* name, const char* value)
{
  Deccfg* cfg = (Deccfg*)user;

#if DEBUG
  printf("Cfg [%s]%s=%s\n", section, name, value);
#endif
  /* Font */
  if(MATCH("font", "file")) {
    cfg->font.file = _cfg_get_path(cfg, value);
  }else if(MATCH("font", "width")) {
    cfg->font.w = _cfg_get_num(cfg, value);
  }else if(MATCH("font", "height")) {
    cfg->font.h = _cfg_get_num(cfg, value);
  }else if(MATCH("font", "scale")) {
    cfg->font.scale = _cfg_get_dbl(cfg, value);
  }else if(MATCH("font", "threshold")) {
    cfg->font.thresh = _cfg_get_num(cfg, value);
  /* Filter */
  }else if(MATCH("filter", "threshold")) {
    cfg->filter.thresh = _cfg_get_num(cfg, value);
  }else if(MATCH("filter", "scale")) {
    cfg->filter.scale = _cfg_get_dbl(cfg, value);
  }else if(MATCH("filter", "blur")) {
    cfg->filter.blur = _cfg_get_dbl(cfg, value);
  /* Edger */
  }else if(MATCH("edger", "threshold")) {
    cfg->edger.thresh = _cfg_get_num(cfg, value);
  }else if(MATCH("edger", "areamin")) {
    cfg->edger.area = _cfg_get_num(cfg, value);
  }else if(MATCH("edger", "adj-x")) {
    cfg->edger.adj_x = _cfg_get_num(cfg, value);
  }else if(MATCH("edger", "adj-y")) {
    cfg->edger.adj_y = _cfg_get_num(cfg, value);
  }else if(MATCH("edger", "adj-w")) {
    cfg->edger.adj_w = _cfg_get_num(cfg, value);
  }else if(MATCH("edger", "adj-h")) {
    cfg->edger.adj_h = _cfg_get_num(cfg, value);
  /* Filter-ex */
  }else if(MATCH("filter-ex", "ksize")) {
    cfg->filterex.ksize = _cfg_get_num(cfg, value);
  }else if(MATCH("filter-ex", "derivative")) {
    cfg->filterex.deriv = _cfg_get_num(cfg, value);
  }else if(MATCH("filter-ex", "mode")) {
    cfg->filterex.mode = _cfg_get_num(cfg, value);
  }else if(MATCH("filter-ex", "scale")) {
    cfg->filterex.scale = _cfg_get_dbl(cfg, value);
  }else if(MATCH("filter-ex", "delta")) {
    cfg->filterex.delt = _cfg_get_dbl(cfg, value);
  }else if(MATCH("filter-ex", "weight")) {
    cfg->filterex.weight = _cfg_get_dbl(cfg, value);
  /* Edger-ex */
  }else if(MATCH("edger-ex", "ksize")) {
    cfg->edgerex.ksize = _cfg_get_num(cfg, value);
  }else if(MATCH("edger-ex", "derivative")) {
    cfg->edgerex.deriv = _cfg_get_num(cfg, value);
  }else if(MATCH("edger-ex", "scale")) {
    cfg->edgerex.scale = _cfg_get_dbl(cfg, value);
  }else if(MATCH("edger-ex", "delta")) {
    cfg->edgerex.delt = _cfg_get_dbl(cfg, value);
  }else if(MATCH("edger-ex", "weight")) {
    cfg->edgerex.weight = _cfg_get_dbl(cfg, value);
  /* Other */
  }else{
    printf("Unknown cfg [%s]%s=%s\n", section, name, value);
  }
}

bool Deccfg::load_cfg(const char* path)
{
  if(ini_parse(path, cfg_handler, this) < 0) {
    return false;
  }
  return true;
}

bool Deccfg::save_cfg(const char* path)
{
  return false;
}

void Deccfg::set_base(const char* _base)
{
  base = _base;
}

void Deccfg::update()
{
  Mat mt, mt_one;

  apply_unsets();

  mt = imread(font.file, CV_LOAD_IMAGE_COLOR);
  int fw, fh;

  fw = mt.cols;
  fh = mt.rows;

  font.fw = fw * font.scale * filter.scale;
  font.fh = font.h * font.scale * filter.scale;

  /* Split out the font into individual Mats */
  for(int y = 0; y < fh; y += font.h) {
    mt_one = mt(Rect(0,y, fw,font.h));
    resize(mt_one, mt_one, Size(0,0), font.scale, font.scale, INTER_CUBIC);
    mt_one = img_process(mt_one, this, 2);
    lns.push_back(mt_one);
    testdisp(mt_one);
  }
}

char* Deccfg::make_path(char* file)
{
  char* out;
  int len;
  len = snprintf(NULL, 0, "%s/%s", base, file);
  out = (char*)malloc(len+1);
  sprintf(out, "%s/%s", base, file);
  return out;
}

int xtoi_full(char* str, int base, char** sstr)
{
  int val = 0;
  int done, hit;
  char c;
  char* ostr;
  int omul = 1;

  ostr = str;
  if(base == -1) base = 10;

  if(*str == '-') {
    str++;
    omul = -1;
  }

  switch(*str) {
    case '0':
      str++;
      switch(*str) {
        case 'x':
          base = 16;
          str++;
          break;
        case 'b':
          base = 2;
          str++;
          break;
        case 'o':
          base = 8;
          str++;
          break;
        default:
          str--;
          break;
      }
      break;
    case '$':
      base = 16;
      str++;
      break;
  }

  hit = 0;
  done = 0;
  while(!done) {
    c = *str++;
    switch(c) {
      case 'a': case 'b':
      case 'c': case 'd':
      case 'e': case 'f':
        if(base < 16) {
          done = 1;
          break;
        }
        val *= base;
        val += (c - 'a') + 0xA;
        break;
      case 'A': case 'B':
      case 'C': case 'D':
      case 'E': case 'F':
        if(base < 16) {
          done = 1;
          break;
        }
        val *= base;
        val += (c - 'A') + 0xA;
        break;
      case '8': case '9':
        if(base < 10) {
          done = 1;
          break;
        }
        /* fall thru */
      case '2': case '3':
      case '4': case '5':
      case '6': case '7':
        if(base < 8) {
          done = 1;
          break;
        }
        /* fall thru */
      case '0': case '1':
        val *= base;
        val += c - '0';
        break;
      default:
        done = 1;
        break;
    }
    if(!done) hit = 1;
  }

  if(!hit) {
    str = ostr+1;
    val = 0;
  }

  if(sstr) *sstr = str-1;
  return val * omul;
}

int xtoi_bs(char* str, int base) { return xtoi_full(str, base, NULL); }
int xtoi(char* str) { return xtoi_full(str, -1, NULL); }
