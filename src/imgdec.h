#ifndef IMGDEC_H_
#define IMGDEC_H_

struct Imgdec {
  Imgdec();
  ~Imgdec();
  //--- Functions
  void set_cfg(Deccfg* cfg);
  void set_source(Imgsrc* src);
  void set_config(Deccfg* cfg);
  int update();

  /* don't use these w */
  int ident_char(Mat mt);
  void split_chars(Mat srcmt, std::vector<Mat>& mts);
  void clean_sort_contours(std::vector<std::vector<Point> > icon, std::vector<Vec4i> ihier,
                           std::vector<std::vector<Point> >& con, std::vector<Vec4i>& hier);

  //--- Members
  Deccfg* cfg;
  Imgsrc* src;

  int value;
};

Mat img_process(Mat srcmt, Deccfg* cfg, int src);
void testdisp(Mat mt);

#endif
