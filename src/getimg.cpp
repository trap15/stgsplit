#include "top.h"
#include <fstream>

#include "getimg.h"

Imgsrc::Imgsrc()
{
  x = 0;
  y = 0;
  w = 0;
  h = 0;
  img = NULL;
}

Imgsrc::~Imgsrc()
{
}

void Imgsrc::acquire(int _x, int _y, int _w, int _h)
{
  x = _x;
  y = _y;
  w = _w;
  h = _h;
}

void Imgsrc::set_img(const char* path)
{
  img = path;
}

Mat Imgsrc::update()
{
  if(img) {
    return update_file();
  }else{
    return update_raw();
  }
}

Mat Imgsrc::update_file()
{
  /* orz streams and c++ */
  std::ifstream file(img);
  std::vector<char> data;

  file >> std::noskipws;
  std::copy(std::istream_iterator<char>(file), std::istream_iterator<char>(), std::back_inserter(data));

  return imdecode(Mat(data), 1);
}

Mat Imgsrc::update_raw()
{
  return Mat();
}
