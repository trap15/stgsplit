#ifndef GETIMG_H_
#define GETIMG_H_

struct Imgsrc {
  Imgsrc();
  ~Imgsrc();
  //--- Functions
  void acquire(int x, int y, int w, int h);
  Mat update();

  /* Don't use these w */
  Mat update_file();
  Mat update_raw();

  void set_img(const char* path); /* Testing only */

  //--- Members
  int x,y;
  int w,h;

  const char* img; /* only for testing */
};

#endif
