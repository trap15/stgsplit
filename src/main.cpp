#include "top.h"
#include <cstdio>

#include "getimg.h"
#include "deccfg.h"
#include "imgdec.h"

int main(int argc, char* argv[])
{
  Deccfg cfg;
  Imgsrc src;
  Imgdec dec;

  int val;

#if DEBUG
  namedWindow("TestWin", CV_WINDOW_AUTOSIZE);
#endif

  cfg.set_base(argv[1]);
  cfg.load_cfg(cfg.make_path("conf.ini"));
  cfg.update();
  dec.set_config(&cfg);

  src.set_img(argv[2]);
  src.acquire(0,0, 0,0);
  dec.set_source(&src);

  val = dec.update();

  printf("Number: %d\n", val);

#if DEBUG
  destroyWindow("TestWin");
#endif

  return EXIT_SUCCESS;
}
