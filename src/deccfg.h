#ifndef DECCFG_H_
#define DECCFG_H_

struct Deccfg {
  Deccfg();
  ~Deccfg();
  //--- Functions
  void set_base(const char* base);

  bool load_cfg(const char* path);
  bool save_cfg(const char* path);

  void update();

  char* make_path(char* file);

  void apply_unsets();
  //--- Members
  const char* base;
  std::vector<Mat> lns;

  /* Params */
  struct {
    const char* file;
    int w, h;
    int fw, fh;
    int scale;
    int thresh;
  } font;

  struct {
    int thresh;
    int mode;
    double blur;
    double scale;
  } filter;

  struct {
    int thresh;
    int area;

    int adj_x,adj_y;
    int adj_w,adj_h;
  } edger;

  struct {
    int deriv;
    int ksize;
    double scale;
    double delt;
    double weight;
    int mode; /* filterex only */
  } filterex, edgerex;
};

#endif
