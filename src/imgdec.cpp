#include "top.h"

#include "getimg.h"
#include "deccfg.h"
#include "imgdec.h"

Imgdec::Imgdec()
{
  src = NULL;
  value = 0;
}

Imgdec::~Imgdec()
{
}

void Imgdec::set_source(Imgsrc* _src)
{
  src = _src;
}

void Imgdec::set_config(Deccfg* _cfg)
{
  cfg = _cfg;
}

bool ident_char_ch(Mat mt, Mat ch, int c_w, int* val);

int Imgdec::update()
{
  Mat mt = src->update();

  std::vector<Mat> mts;
  split_chars(mt, mts);

  value = 0;
  int i = 0;
  for(std::vector<Mat>::iterator it = mts.begin(); it != mts.end(); it++, i++) {
    value *= 10;
    value += ident_char(*it);
  }

  return value;
}

bool ident_char_lv(Mat mt, Mat ln, int* val, Deccfg* cfg)
{
  Mat res;
  Mat xln;

  testdisp(mt);

  printf("%d,%d vs %d,%d\n", ln.cols,ln.rows, mt.cols,mt.rows);

  ln.copyTo(xln);
  matchTemplate(ln, mt, res, cfg->filterex.mode);
  normalize(res, res, 0, 1, NORM_MINMAX, -1, Mat());

  double minval, maxval;
  Point minloc, maxloc;
  Point matchloc;

  minMaxLoc(res, &minval, &maxval, &minloc, &maxloc, Mat());
  if(cfg->filterex.mode == CV_TM_SQDIFF || cfg->filterex.mode == CV_TM_SQDIFF_NORMED)
    matchloc = minloc;
  else
    matchloc = maxloc;

  if(matchloc.x == 0 && matchloc.y == 0)
    return false;

  rectangle(xln, matchloc, Point(matchloc.x + ln.cols, matchloc.y + ln.rows), Scalar::all(0), CV_FILLED, 8, 0);

  double dval = (double)matchloc.x / (double)(cfg->font.scale*cfg->font.w*cfg->filter.scale);
  printf("val:%f\n", dval);
  *val = round(dval);

  return true;
}

int Imgdec::ident_char(Mat mt)
{
  int val;
  for(int i = 0; i < cfg->lns.size(); i++) {
    if(ident_char_lv(mt, cfg->lns[i], &val, cfg))
      return val;
  }

  return 0;
}

RNG rng(12345);
void Imgdec::split_chars(Mat srcmt, std::vector<Mat>& mts)
{
  std::vector<std::vector<Point> > contours, ccon;
  std::vector<Vec4i> hierarchy, chier;

  Mat ctrmt = img_process(srcmt.clone(), cfg, 1);
  srcmt = img_process(srcmt, cfg, 0);

  testdisp(srcmt);
  testdisp(ctrmt);

  findContours(ctrmt, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0,0));
  clean_sort_contours(contours, hierarchy, ccon, chier);

  int i = 0;
  for(std::vector<std::vector<Point> >::iterator it = ccon.begin(); it != ccon.end(); i++, it++) {
    int area = contourArea(*it);
    Rect bnd = boundingRect(*it);
  }

  Mat mt;
  mt = Mat::zeros(srcmt.size(), CV_8UC3);
  for(int i = 0; i < (int)ccon.size(); i++) {
    Scalar color = Scalar(rng.uniform(80,255), rng.uniform(80,255), rng.uniform(80,255));
    drawContours(mt, ccon, i, color, 2, 8, chier, 0, Point());
  }
  testdisp(mt);

  for(std::vector<std::vector<Point> >::iterator it = ccon.begin(); it != ccon.end(); it++) {
    Rect bnd = boundingRect(*it);
    bnd.x += cfg->edger.adj_x;
    bnd.y += cfg->edger.adj_y;
    bnd.width += cfg->edger.adj_w;
    bnd.height += cfg->edger.adj_h;

    if(bnd.x < 0) bnd.x = 0;
    if(bnd.y < 0) bnd.y = 0;
    if(bnd.width < 0) bnd.width = 0;
    if(bnd.height < 0) bnd.height = 0;

    if(bnd.width  > srcmt.cols-bnd.x) bnd.width  = srcmt.cols-bnd.x;
    if(bnd.height > srcmt.rows-bnd.y) bnd.height = srcmt.rows-bnd.y;

    Mat tmt = srcmt(bnd).clone();
    double w = cfg->font.fh * tmt.cols / tmt.rows;
    resize(tmt, tmt, Size(w,cfg->font.fh), 0,0, INTER_LANCZOS4);
    mts.push_back(tmt);
  }
}

bool contour_sort(std::vector<Point> l, std::vector<Point> r)
{
  Rect l_bnd, r_bnd;
  l_bnd = boundingRect(l);
  r_bnd = boundingRect(r);
  return l_bnd.x < r_bnd.x;
}

void Imgdec::clean_sort_contours(std::vector<std::vector<Point> > icon, std::vector<Vec4i> ihier,
                                 std::vector<std::vector<Point> >& con, std::vector<Vec4i>& hier)
{
  Vec4i vec;
  vec[0] = vec[1] = vec[2] = vec[3] = -1;

  for(int i = 0, l = 0; i < icon.size(); i++) {
    int area = contourArea(icon[i]);
    if(area < cfg->edger.area)
      continue;

    if(ihier[i][3] != -1)
      continue;

    vec[1] = l - 1;
    vec[0] = l;
    con.push_back(icon[i]);
    hier.push_back(vec);
    l++;
  }

  std::sort(con.begin(), con.end(), contour_sort);
}

#define EDGER_BORDER BORDER_CONSTANT

Mat img_process(Mat srcmt, Deccfg* cfg, int src)
{
  Mat edge, edged;
  int scl;
  double blsz;
  int thresh, deriv, ksize;
  double sscale;
  double delt;
  double weight;

  scl = cfg->filter.scale;
  blsz = cfg->filter.blur;
  switch(src) {
    case 0: /* Filter */
      thresh = cfg->filter.thresh;
      deriv = cfg->filterex.deriv;
      ksize = cfg->filterex.ksize;
      sscale = cfg->filterex.scale;
      delt = cfg->filterex.delt;
      weight = cfg->filterex.weight;
      break;
    case 1: /* Edger */
      thresh = cfg->edger.thresh;
      deriv = cfg->edgerex.deriv;
      ksize = cfg->edgerex.ksize;
      sscale = cfg->edgerex.scale;
      delt = cfg->edgerex.delt;
      weight = cfg->edgerex.weight;
      break;
    case 2: /* Font */
      thresh = cfg->font.thresh;
      deriv = cfg->filterex.deriv;
      ksize = cfg->filterex.ksize;
      sscale = cfg->filterex.scale;
      delt = cfg->filterex.delt;
      weight = cfg->filterex.weight;
      break;
  }

  if(scl != 0)
    resize(srcmt, srcmt, Size(0,0), scl,scl, INTER_LANCZOS4);

  cvtColor(srcmt, srcmt, CV_RGB2GRAY);
  threshold(srcmt, srcmt, thresh, 255, 3);

  Mat grad_x, grad_y;
  Mat abs_grad_x, abs_grad_y;

  Sobel(srcmt, grad_x, CV_16S, deriv, 0, ksize, sscale, delt, EDGER_BORDER);
  convertScaleAbs(grad_x, abs_grad_x);

  Sobel(srcmt, grad_y, CV_16S, 0, deriv, ksize, sscale, delt, EDGER_BORDER);
  convertScaleAbs(grad_y, abs_grad_y);

  addWeighted(abs_grad_x, weight, abs_grad_y, weight, 0, edged);

  if(blsz != 0)
    blur(edged, edged, Size(blsz,blsz));

  return edged;
}

void testdisp(Mat mt)
{
#if DEBUG
  imshow("TestWin", mt);
  waitKey(0);
#endif
}
