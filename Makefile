PPATH    =
PREFIX   =
CC       = $(PREFIX)gcc
CXX      = $(PREFIX)g++
AS       = $(PREFIX)as
LD       = $(PREFIX)ld
OBJCOPY  = $(PREFIX)objcopy

OBJECT   = getimg.o deccfg.o imgdec.o main.o ini.o

OUTPUT   = stgsplit

OBJECTS  = $(foreach file,$(OBJECT),obj/$(file))

CFLAGS   = -Isrc/ -fomit-frame-pointer -Wall -Wextra
CXXFLAGS =
SFLAGS   =
LDFLAGS  = -lm

CFLAGS  += `pkg-config --cflags opencv`
LDFLAGS += `pkg-config --libs opencv`

LDFLAGS += -g
CFLAGS  += -g -O0 -DDEBUG=1

.PHONY: all clean release rebuild

all: $(OBJECTS) $(OUTPUT)

obj/%.o: src/%.c
	$(CC) $(CFLAGS) -c -o $@ $<
obj/%.o: src/%.cpp
	$(CXX) $(CFLAGS) $(CXXFLAGS) -c -o $@ $<
$(OUTPUT): $(OBJECTS)
	$(CXX) $(LDFLAGS) -o $@ $(OBJECTS)

rebuild: clean all

define \n


endef

release:
	$(foreach file,$(OBJECTS),@echo " RELEASE  $(file)" ${\n}@$(RM) $(file) ${\n})
clean:
	$(foreach file,$(OBJECTS),@echo " CLEAN    $(file)" ${\n}@$(RM) $(file) ${\n})
	$(foreach file,$(OUTPUT),@echo " CLEAN    $(file)" ${\n}@$(RM) $(file) ${\n})

